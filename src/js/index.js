import { states as licensePlates } from './states.js';

import 'bootstrap';
import Vue from 'vue';

let seenStates = JSON.parse(localStorage.getItem('seenStates')) ?? [];

let app = new Vue({
    el: '#app',
    data: {
        licensePlates,
        seenStates,
    },
    watch: {
        seenStates: {
            handler: function (seen) {
                localStorage.setItem('seenStates', JSON.stringify(seen));
            },
            deep: true,
        },
    },
    template: `
        <div>
            <div class="sticky-top mb-3 bg-dark text-light rounded p-3 px-5 my-3 d-flex flex-wrap justify-content-end">
                <div class="mx-3">Seen {{ seenStates.length }}/{{ licensePlates.length }}</div>
                <div class="mx-3"><button class="btn btn-sm btn-danger" v-on:click="seenStates = []">Reset</button></div>
            </div>

            <div class="row mb-3">
                <div class="d-grid gap-2 mb-1 col-6 col-lg-4 col-xl-3"
                v-for="licensePlate in licensePlates"
                v-bind:key="licensePlate.abbreviation">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" v-bind:value="licensePlate.abbreviation" v-bind:id="'btn-' + licensePlate.name" v-model="seenStates" autocomplete="off">
                        <label class="form-check-label" v-bind:for="'btn-' + licensePlate.name">
                            {{ licensePlate.name }}
                        </label>
                    </div>
                </div>
            </div>
        </div>
    `,
});


let snoop_message = `
         ###################
         *7****7####M*****7*
               |####M
  |###         |####M
  |###         |####M
  ]###.        |####M                               Ted Tramonte, LLC
#########M     |####M
**%###C**"     |####M                                  Hey, there!
  |###         |####M             If you're the type to pop open the hood of my website,
  |###         |####M              there's a very good chance I'd like to talk to you,
  |###         |####M                              work related or not.
  |###         {####M
  |###         #####M                      https://www.tedtramonte.com/contact
  |###b       [####b
    ###M....,,####C
     |####m#####M
`

window.addEventListener('load', function () {
    console.log(snoop_message);
});
