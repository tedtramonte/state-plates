const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  devtool: (process.env.NODE_ENV === 'development') ? 'inline-source-map' : false,
  mode: process.env.NODE_ENV,
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  entry: {
    index: [
      './src/js/index.js',
      './src/scss/style.scss',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'public', 'assets'),
    filename: "js/[name].js",
    publicPath: "/"
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/style.css',
    })
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        use:
          [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { sourceMap: true } },
            { loader: 'sass-loader', options: { sourceMap: true } },
          ],
      },
    ],
  },
};
