# State Plates
[State Plates](https://tedtramonte.gitlab.io/state-plates/) is a simple web application to track license plates you see on a road trip written using Vue.js.

[Play now!](https://tedtramonte.gitlab.io/state-plates/)
